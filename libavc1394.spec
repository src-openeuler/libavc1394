Name:           libavc1394
Version:        0.5.4
Release:        12
Summary:        Control IEEE 1394 audio/video devices
License:        GPLv2+ and LGPLv2+
URL:            http://sourceforge.net/projects/libavc1394/
Source:         https://sourceforge.net/projects/libavc1394/files/libavc1394/libavc1394-%{version}.tar.gz
BuildRequires:  libraw1394-devel chrpath gcc

%description
Libavc1394 is a programming interface for the 1394 Trade Association
AV/C (Audio/Video Control) Digital Interface Command Set.It is intended
for use with GNU/Linux IEEE-1394.

%package devel
Summary: Development libs for libavc1394

Requires: %{name} = %{version}-%{release} libraw1394-devel pkgconfig

%description devel
The devel package contains the libavc1394 library and the include files.

%package help
Summary: Help documents for libavc1394

%description help
Man pages and other related help documents for libavc1394.

%prep
%autosetup -n %{name}-%{version} -p1
chmod -x test/dvcont.c

%build
%configure
%make_build

%install
%make_install
%delete_la_and_a
chrpath -d $RPM_BUILD_ROOT%{_libdir}/lib*
chrpath -d $RPM_BUILD_ROOT%{_bindir}/*

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%license COPYING
%doc AUTHORS
%{_libdir}/{libavc1394,librom1394}.so.*
%{_bindir}/{dvcont,mkrfc2734,panelctl}

%files devel
%{_includedir}/libavc1394/
%{_libdir}/{libavc1394,librom1394}.so
%{_libdir}/pkgconfig/libavc1394.pc

%files help
%doc NEWS README ChangeLog TODO
%{_mandir}/man1/*

%changelog
* Thu Feb 20 2020 cangyi <cangyi@huawei.com> - 0.5.4-12
- Package init
